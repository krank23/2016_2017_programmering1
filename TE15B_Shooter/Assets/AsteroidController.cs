﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class AsteroidController : MonoBehaviour
{

    [SerializeField]
    float speed = 2f;

    Vector3 movement;

    // Use this for initialization
    void Start()
    {
        movement = Vector3.down * Random.Range(speed, speed * 2);

        float xPos = Random.Range(-4f, 4f);

        transform.position = new Vector3(xPos, 6, 0);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(movement * Time.deltaTime);

        if (transform.position.y < -6)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.gameObject.tag == "bullet")
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");

            PlayerController pc = player.GetComponent<PlayerController>();

            pc.AddPoints(500);
            pc.PlayExplodeSound();

            Destroy(this.gameObject);

        }

    }
}
