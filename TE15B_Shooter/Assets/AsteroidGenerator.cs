﻿using UnityEngine;
using System.Collections;

public class AsteroidGenerator : MonoBehaviour
{

    [SerializeField]
    GameObject asteroidPrefab;

    [SerializeField]
    float coolDownMax;

    float coolDownValue;

	// Use this for initialization
	void Start ()
	{
	    coolDownValue = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    coolDownValue += Time.deltaTime;

	    if (coolDownValue > coolDownMax)
	    {
	        Instantiate(asteroidPrefab);
	        coolDownValue = 0;
	    }
	}
}
