﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    GameObject bulletPrefab;

    [SerializeField]
    Transform weaponPoint;

    [SerializeField]
    AudioClip lazer;

    [SerializeField]
    AudioClip explosion;

    [SerializeField]
    Text scoreText;

    float coolDownValue = 0;
    float coolDownMax = .3f;

    int points = 0;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        float speed = 0.1f;

        //Debug.Log("Jag uppdateras!");

        //Debug.Log(transform.position.x);

        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");

        //Debug.Log(moveX);


        Vector3 movementX = new Vector3(moveX, 0, 0) * speed;
        Vector3 movementY = new Vector3(0, moveY, 0) * speed;

        transform.Translate(movementX + movementY);

        if (transform.position.x > 5 || transform.position.x < -5)
        {
            transform.Translate(-movementX);
        }

        if (transform.position.y > 4.5f || transform.position.y < -4.5f)
        {
            transform.Translate(-movementY);
        }

        coolDownValue += Time.deltaTime;

        if (Input.GetAxisRaw("Fire1") > 0 && coolDownValue > coolDownMax)
        {
            AudioSource speaker = this.GetComponent<AudioSource>();
            speaker.PlayOneShot(lazer);

            Instantiate(bulletPrefab, weaponPoint.position, Quaternion.identity);
            coolDownValue = 0;
        }

    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "enemy")
        {
            SceneManager.LoadScene(1);
            //Application.LoadLevel(1);
        }
    }

    public void AddPoints(int amount)
    {
        points += amount;

        scoreText.text = points + "";

    }

    public void PlayExplodeSound()
    {
        AudioSource speaker = this.GetComponent<AudioSource>();
        speaker.PlayOneShot(explosion);
    }

}
