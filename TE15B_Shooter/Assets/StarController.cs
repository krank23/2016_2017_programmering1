﻿using UnityEngine;
using System.Collections;

public class StarController : MonoBehaviour {

    Vector3 movement;

	// Use this for initialization
	void Start () {
        float xPos = Random.Range(-4.5f, 4.5f);
        float yPos = Random.Range(-5f, 5f);

        transform.position = new Vector3(xPos, yPos, 0);

        movement = Vector3.down * Random.Range(3f, 6f);
        
        transform.localScale = Vector3.one * Random.Range(0.1f, 0.2f);

	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate(movement * Time.deltaTime);

        if (transform.position.y < -5)
        {
            float newX = Random.Range(-4f, 4f);
            transform.position = new Vector3(newX, 5, 0);
        }
	}


}
