﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float speed = 0.2f;

        Vector3 movement = new Vector3(0, speed, 0);

        transform.Translate(movement);

        if (transform.position.y > 5)
        {
            Destroy(this.gameObject);
        }
	}

    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.gameObject.tag == "enemy")
        {
            Destroy(this.gameObject);



        }

    }
}
