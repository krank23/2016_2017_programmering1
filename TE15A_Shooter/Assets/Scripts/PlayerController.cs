﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Windows.Speech;

public class PlayerController : MonoBehaviour {


    [SerializeField]
    GameObject bulletPrefab;

    [SerializeField]
    Transform weapon;

    [SerializeField]
    Text scoreBox;

    [SerializeField]
    AudioClip pewpew;

    [SerializeField]
    AudioClip boom;

    float coolDownMax = 0.5f;
    float coolDownValue = 0;

    int points = 0;

	// Use this for initialization
	void Start () {
        AddPoints(1000);
	}
	
	// Update is called once per frame
	void Update () {

        float speed = 0.1f;

        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");

        Vector3 movementX = new Vector3(moveX, 0, 0) * speed;
        Vector3 movementY = new Vector3(0, moveY, 0) * speed;

        transform.Translate(movementX + movementY);

        if (transform.position.x > 3.5f || transform.position.x < -3.5f)
        {
            transform.Translate(-movementX);
        }

        if (transform.position.y > 3.5f || transform.position.y < -3.5f)
        {
            transform.Translate(-movementY);
        }

        coolDownValue += Time.deltaTime;

        if (Input.GetAxisRaw("Fire1") > 0 && coolDownValue > coolDownMax)
        {
            AudioSource speaker = GetComponent<AudioSource>();
            speaker.PlayOneShot(pewpew);

            coolDownValue = 0;
            Instantiate(bulletPrefab, weapon.position, Quaternion.identity);
        }

	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "enemy")
        {
            SceneManager.LoadScene(1);
        }
    }

    public void AddPoints(int amount)
    {
        points += amount;

        scoreBox.text = points + "p";
    }

    public void PlayBoom()
    {
        AudioSource speaker = GetComponent<AudioSource>();
        speaker.PlayOneShot(boom);
    }

}
