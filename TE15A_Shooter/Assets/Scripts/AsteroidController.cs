﻿using UnityEngine;
using System.Collections;

public class AsteroidController : MonoBehaviour
{

    [SerializeField]
    float speed = 2;

    [SerializeField]
    GameObject explosion;

    Vector3 movement;

    // Use this for initialization
    void Start()
    {

        float xPos = Random.Range(-4.5f, 4.5f);

        transform.position = new Vector3(xPos, 6, 0);

        movement = Vector3.down * speed;

    }

    // Update is called once per frame
    void Update()
    {

        transform.Translate(movement * Time.deltaTime);

        if (transform.position.y < -6)
        {
            Destroy(this.gameObject);
        }

    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "laser")
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");

            PlayerController pc = player.GetComponent<PlayerController>();

            pc.AddPoints(500);
            //pc.PlayBoom();
            Instantiate(explosion, transform.position,Quaternion.identity);

            Destroy(this.gameObject);
        }
    }
}
