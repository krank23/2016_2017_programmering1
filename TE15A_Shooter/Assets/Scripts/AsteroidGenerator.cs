﻿using UnityEngine;
using System.Collections;

public class AsteroidGenerator : MonoBehaviour {

    [SerializeField]
    GameObject asteroidPrefab;

    float cooldownValue = 0f;
    float cooldownMax = 2f;
	
	void Update () {

        cooldownValue += Time.deltaTime;

        if (cooldownValue > cooldownMax)
        {
            Instantiate(asteroidPrefab);
            cooldownValue = 0;
        }

	}
}
