﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float speed = 0.1f;

        Vector3 movementY = Vector3.up * speed;

        transform.Translate(movementY);

        if (transform.position.y > 6f)
        {
            Destroy(gameObject);
        }

    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "enemy")
        {
            Destroy(this.gameObject);
        }
    }

}
