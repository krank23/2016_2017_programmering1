﻿using UnityEngine;
using System.Collections;

public class StarController : MonoBehaviour {

    float speed = 2;

	// Use this for initialization
	void Start () {

        float xPos = Random.Range(-4.5f, 4.5f);
        float yPos = Random.Range(-5f, 5f);

        transform.position = new Vector3(xPos, yPos, 0);

        speed = Random.Range(2f, 10f);

        float size = Random.Range(0.1f, 0.2f);

        transform.localScale = new Vector3(size, size, 1);

	}
	
	// Update is called once per frame
	void Update () {

        Vector3 movement = Vector3.down * speed * Time.deltaTime;

        transform.Translate(movement);

        if (transform.position.y < -5)
        {
            float xPos = Random.Range(-4.5f, 4.5f);

            transform.position = new Vector3(xPos, 5, 0);
        }

	}
}
