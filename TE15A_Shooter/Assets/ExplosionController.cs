﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour
{
    // En variabel som pekar mot partikelsystemet
    private ParticleSystem ps;

    void Start()
    {
        // Hämta en referens till partikelsystemet; spara referensen i variabeln ps.
        ps = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        // Om partikelsystemet just nu inte gör något, förstör objektet
        if (!ps.isPlaying)
        {
            Destroy(this.gameObject);
        }
    }
}
